===========
envfrom2srs
===========

------------------------------------------------------------
rewrites the envelope sender address in view of remailing it
------------------------------------------------------------

:Datum: 2014-03-17
:Version: 1.0
:Manual section: 1

SYNOPSIS
========
*envfrom2srs* [`fwdomain`] `email`


DESCRIPTION
===========
Rewrite the `email` with an SRS encoded version.
`fwdomain` is used as forward domain.


.. warning ::
        Use only if absolutely necessary.  It is *very* inefficient and a security risk.

Config File
===========
The program uses `/etc/mail/pysrs.cfg`, if available::

     [srs]
     secret = 'shhhh!'
     maxage = 8
     hashlength = 8
     fwdomain =  'mydomain.com'
     separator = '='

SEE ALSO
========
``srs2envtol``\(1)


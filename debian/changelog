pysrs (1.0.4-2) unstable; urgency=medium

  * Fix autopkgtests.

 -- Sandro Knauß <hefee@debian.org>  Thu, 18 Aug 2022 23:40:27 +0200

pysrs (1.0.4-1) unstable; urgency=medium

  * New upstream release (Closes: #1008829, #994192)
  * Set Rules-Requires-Root to no.
  * Set section:utils for pysrs-bin.
  * Add autopkgtests.
  * Add python3-milter to Suggests.
  * Remove upstream applied patch.
  * Update install files.
  * Add python3-setuptools to Build-Depends.
  * Update Standards-Version (no changes needed).
  * Update copyright info for debian files.
  * Replace link to github repo for source.

 -- Sandro Knauß <hefee@debian.org>  Tue, 21 Jun 2022 02:14:51 +0200

pysrs (1.0.3-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Sandro Knauß ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.
  * Enable Salsa CI.
  * Install egg-info file.
  * Do not install python bytecode.

 -- Sandro Knauß <hefee@debian.org>  Mon, 20 Sep 2021 13:27:24 +0200

pysrs (1.0.3-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Knauß ]
  * Replace own patch with upstream one to correct the version numbers.
  * Bump to debhelper 12 (No changes needed).
  * Bump to Standards-Version to 4.4.0 (No changes needed).
  * Switch Uploaders and Maintainer field.
  * Remove python2 support from package (Closes: #937545)
  * Ignore d/files.

 -- Sandro Knauß <hefee@debian.org>  Fri, 30 Aug 2019 12:50:18 +0200

pysrs (1.0.3-1) unstable; urgency=medium

  * new upstream release.
  * Bump Standards-Version to 4.1.2 (No changes needed).

 -- Sandro Knauß <hefee@debian.org>  Sat, 16 Dec 2017 16:12:32 +0100

pysrs (1.0.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Sandro Knauß ]
  * new upstream release.
  * Bump compat level to 10 (No changes needed).
  * Bump Standards-Version to 4.1.1:
    - Update Copyright format link to use secure version.
  * Use secure version for upstream source, where possible.
  * Sort blocks in copyright correctly.
  * Change my own mailaddress to hefee@debian.org
  * Add new package: python3-srs
  * Add new package: pysrs-bin
  * Switch to pybuild.
  * python-bsddb3 is needed for SRS/DB.py
  * Get rid of X-Python*-Version parts
  * Move binaries to pysrs-bin package.
  * Update watch file.

 -- Sandro Knauß <hefee@debian.org>  Sat, 04 Nov 2017 11:31:47 +0100

pysrs (1.0-2) unstable; urgency=medium

  [ Scott Kitterman ]
  * Add missing uploaders and Vcs-* fields in debian/control

  [ Sandro Knauß ]
  * Update package description mentioning SES and SocketMap

 -- Sandro Knauß <bugs@sandroknauss.de>  Thu, 14 Aug 2014 23:50:00 +0200

pysrs (1.0-1) unstable; urgency=medium

  [ Sandro Knauß ]
  * Initial release. (Closes: 740865)

  [ Scott Kitterman ]
  * Add direct build-dep on dh-python so the current version is used instead
    of the version included in python-defaults
  * Update SRS protocol description URL to current location at openspf.org

 -- Sandro Knauß <bugs@sandroknauss.de>  Sun, 20 Jul 2014 15:05:19 +0200
